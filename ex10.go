package main

import "fmt"

func main() {
	var chartype int8 = 'R' 
	var letter rune = 'Ї' // це для символа треба

	fmt.Printf("Code '%c' - %d\n", chartype, chartype)
	fmt.Printf("%c", letter)

	//Задание.
	//1. Вывести украинскую букву 'Ї'
	//2. Пояснить назначение типа "rune"
}
