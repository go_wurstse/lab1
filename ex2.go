package main

import "fmt"

func main() {
	//Все переменные инициализируются значением по умолчанию, т.е. 0
	var defaultInt8 int8
	var defaultInt16 int16
	var defaultInt32 int32
	var defaultInt64 int64
	var defaultInt int

	fmt.Println("Default values (signed): ", defaultInt8, defaultInt16, defaultInt32, defaultInt64, defaultInt)

	var defaultuInt8 float32
	var defaultuInt16 float32
	var defaultuInt32 float32
	var defaultuInt64 float64
	var defaultuInt float64

	fmt.Println("Default values (unsigned): ", defaultuInt8, defaultuInt16, defaultuInt32, defaultuInt64, defaultuInt)

	//Задание.
	//1. Создать целочисленную переменную (результат не отображать)
}
