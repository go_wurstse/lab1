package main

import "fmt"

func main() {
	var first, second bool
	var third bool = true
	fourth := !third
	var fifth = true

	fmt.Println("first  = ", first)       // false
	fmt.Println("second = ", second)      // false
	fmt.Println("third  = ", third)       // true - це через попереднє присвоєнн значення
	fmt.Println("fourth = ", fourth)      // false - протилежний true
	fmt.Println("fifth  = ", fifth, "\n") // true - як third

	fmt.Println("!true  = ", !true)        // false - ! знак заперечення
	fmt.Println("!false = ", !false, "\n") // true - теж

	fmt.Println("true && true   = ", true && true)         // true - обидва тру
	fmt.Println("true && false  = ", true && false)        // false - одне фолс тому фолс
	fmt.Println("false && false = ", false && false, "\n") // false - обидва фолс

	fmt.Println("true || true   = ", true || true)         // true - обидва тру
	fmt.Println("true || false  = ", true || false)        // true - є одне тру
	fmt.Println("false || false = ", false || false, "\n") // false - тру немає

	fmt.Println("2 < 3  = ", 2 < 3)        // true - 3 більше
	fmt.Println("2 > 3  = ", 2 > 3)        // false - 2 не більше
	fmt.Println("3 < 3  = ", 3 < 3)        // false - вони рівні
	fmt.Println("3 <= 3 = ", 3 <= 3)       // true - рівність дійсна
	fmt.Println("3 > 3  = ", 3 > 3)        // false - рівні!
	fmt.Println("3 >= 3 = ", 3 >= 3)       // true - !!!!
	fmt.Println("2 == 3 = ", 2 == 3)       // false - не дорівнюють
	fmt.Println("3 == 3 = ", 3 == 3)       // true - дорівнюють
	fmt.Println("2 != 3 = ", 2 != 3)       // true - не дорівнюють, але тут навпаки
	fmt.Println("3 != 3 = ", 3 != 3, "\n") // false - як минулий раз

	//Задание.
	//1. Пояснить результаты операций
}
